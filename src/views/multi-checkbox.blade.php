<?php
    $auto_attr = [
        'id' => $name,
        'class' => 'form-control',
        'section_class' => 'form-group',
        'label_class' => 'field select',
        'checkbox_container_class' => 'mt-checkbox-list',
        'checkbox_label_class' => 'mt-checkbox',

    ];

    $attributes = array_merge($auto_attr, $attributes);

    if (! array_key_exists('show_errors', $attributes) || $attributes['show_errors']){
        $attributes['class'] .=  ($errors->has($name) ? ' br-danger' : '' );
    }

    if(!$value || !is_array($value)){
        $value = array($value);
    }

?>

<div class="{{ $attributes['section_class'] }}">
    @if(isset($label) && ! empty($label))
        <label for="{{ $name }}" class="{{ $attributes['label_class'] }}">{{$label}}</label>
    @endif
    <div class="{{ $attributes['checkbox_container_class'] }}">
    @foreach($options as $k => $v)
        <label class="{{ $attributes['checkbox_label_class'] }}"> {{ $v }}
            {!! Form::checkbox($name, $k, in_array($k, $value)) !!}
            <span></span>
        </label>
    @endforeach
    </div>
</div>