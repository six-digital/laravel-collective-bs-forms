<?php
    $auto_attr = [
        'id' => $name,
        'class' => 'form-control' . ($errors->has($name) ? ' br-danger' : '' ),
        'placeholder' => $placeholder
    ];
    $attributes = array_merge($auto_attr, $attributes);
?>

{{--<div class="form-group form-md-line-input form-md-floating-label">--}}
    <span class="date" id="{{ "{$attributes['id']}-holder" }}">
        {{ Form::Text($name, $value, $attributes) }}

        {{--<div class="input-group-addon cursor">--}}
            {{--<i class="datepicker-button fa fa-calendar"></i>--}}
        {{--</div>--}}
    </span>
{{--</div>--}}