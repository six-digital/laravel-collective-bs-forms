@extends('layouts.default')

@section('content')
    {!! Form::Model($object, ['role' => 'form', 'class' => 'form', 'id' => $form_id]) !!}

        {{--DISPLAY THE HEADER--}}
        @if(! isset($crud_include_heading) or $crud_include_heading)
            @yield('crud_heading_content')
        @endif

        {{--DISPLAY CONTENT--}}
        @yield('crud_form_content')

        {{--DISPLAY THE FOOTER--}}
        @if(! isset($crud_include_footer) or $crud_include_footer)
            @yield('crud_footer_content')
        @endif

    {!! Form::Close() !!}
@stop