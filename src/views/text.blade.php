<?php
    $auto_attr = [
        'id' => $name,
        'class' => 'form-control',
        'label_class' => 'field',
        'placeholder' => $placeholder,
        'section_class' => 'form-group'
    ];

    $attributes = array_merge($auto_attr, $attributes);

    if (! array_key_exists('show_errors', $attributes) || $attributes['show_errors']){
        $attributes['class'] .=  ($errors->has($name) ? ' br-danger' : '' );
    }
?>



{{--<div class="{{ $attributes['section_class'] }}">--}}
    {{--<label>Email Address</label>--}}
    {{--<div class="input-group">--}}
        {{--<span class="input-group-addon">--}}
            {{--<i class="fa fa-envelope"></i>--}}
        {{--</span>--}}
        {{--<input type="text" class="form-control" placeholder="Email Address"> </div>--}}
{{--</div>--}}


<div class="{{ $attributes['section_class'] }}">
    @if(isset($label) && ! empty($label))
    <label for="{{ $name }}" class="{{ $attributes['label_class'] }}">{{$label}}</label>
    @endif
    {{ Form::Text($name, $value, $attributes) }}
</div>