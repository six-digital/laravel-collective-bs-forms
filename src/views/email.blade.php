<?php
    $auto_attr = [
        'id' => $name,
        'class' => 'form-control' . ($errors->has($name) ? ' br-danger' : '' ),
        'label_class' => 'field',
        'placeholder' => $placeholder
    ];
    $attributes = array_merge($auto_attr, $attributes);
?>

<div class="{{ $attributes['section_class'] }}">
    @if(isset($label) && ! empty($label))
        <label for="{{ $name }}" class="{{ $attributes['label_class'] }}">{{$label}}</label>
    @endif
    {{ Form::Email($name, $value, $attributes) }}
</div>