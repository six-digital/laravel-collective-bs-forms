<?php
/**
 * Created by PhpStorm.
 * User: jean
 * Date: 2017/03/13
 * Time: 11:21 PM
 */

namespace SixDigital\BsForms;

use Collective\Html\FormBuilder;
use Illuminate\Support\ServiceProvider;

class BsFormsServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadViewsFrom(__DIR__ . '/views', 'sd-bs-forms');

        $this->publishes([
            __DIR__.'/views' => resource_path('views/vendor/sd-bs-forms'),
        ]);

    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        FormBuilder::component('bsText', 'sd-bs-forms::text', ['name', 'value' => null, 'label' => null, 'placeholder' => null, 'attributes' => []]);
        FormBuilder::component('bsTextarea', 'sd-bs-forms::textarea', ['name', 'value' => null, 'label' => null, 'placeholder' => null, 'attributes' => []]);
        FormBuilder::component('bsNumber', 'sd-bs-forms::number', ['name', 'value' => null, 'label' => null, 'placeholder' => null, 'attributes' => []]);
        FormBuilder::component('bsSelect', 'sd-bs-forms::select', ['name', 'options' => [], 'value' => null, 'label' => null, 'attributes' => []]);
        FormBuilder::component('bsEmail', 'sd-bs-forms::email', ['name', 'value' => null, 'label' => null, 'placeholder' => null, 'attributes' => []]);
        FormBuilder::component('bsPassword', 'sd-bs-forms::password', ['name', 'value' => null, 'label' => null, 'placeholder' => null, 'attributes' => []]);
        FormBuilder::component('bsDatePicker', 'sd-bs-forms::date-picker', ['name', 'value' => null, 'label' => null, 'placeholder' => null, 'attributes' => []]);
        FormBuilder::component('bsMultiCheckbox', 'sd-bs-forms::multi-checkbox', ['name', 'options' => [], 'value' => null, 'label' => null, 'attributes' => []]);
    }
}