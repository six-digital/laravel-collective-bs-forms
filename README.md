# **Six Digital - Laravel Collective Bootstrap Extension**

# Introduction
This package provides prestyled templates integrated to render bootstrap form components. 

It is an extension of `laravel/collective`.

-------------------

# Installation

## 1 - Add to Composer

Add this code to `composer.json`

```
    "repositories": [
        {
            "url": "https://git@bitbucket.org/six-digital/laravel-collective-bs-forms.git",
            "type": "vcs"
        }
    ],
    "require": {
        "six-digital/laravel-collective-bs-forms": "dev-master"
    }
```

Then run `composer update`


## 2 - Register the Service Provider

In `config/app.php` add the following code:

```
    'providers' => [
        \SixDigital\BsForms\BsFormsServiceProvider::class,
    ]
```

-------------------
# Features
* Bootstrap Text
* Bootstrap Select
* Bootstrap Email
* Bootstrap Password
* Bootstrap DatePicker
* Bootstrap MultiCheckbox

-------------------

# Usage

After installation, you can use the bootstrap components in your blade views.

### Bootstrap Text
`{!! Form::bsText('name', 'value', 'label', 'placeholder', $attributes = []) !!}`
### Bootstrap Select
`{!! Form::bsSelect('name', $options = [], 'value', 'label', $attributes = []) !!}`
### Bootstrap Email
`{!! Form::bsEmail('name', 'value', 'label', 'placeholder', $attributes = []) !!}`
### Bootstrap Password
`{!! Form::bsPassword('name', 'value', 'label', 'placeholder', $attributes = []) !!}`
### Bootstrap DatePicker
`{!! Form::bsDatePicker('name', 'value', 'label', 'placeholder', $attributes = []) !!}`
### Bootstrap MultiCheckbox
`{!! Form::bsMultiCheckbox('name', $options = [], 'value', 'label', 'placeholder', $attributes = []) !!}`

-------------------

# Customisation

Currently customisation is only available on the template level.

## 1 - Publish the Views
Run the publish command: `php artisan vendor:publish`

This will create copies of the templates into `resources/views/vendor/sd-bs-forms/`

## 2 - Edit the components
There are a number of files in the `resources/views/vendor/sd-bs-forms/` folder.

*Figure it out.*